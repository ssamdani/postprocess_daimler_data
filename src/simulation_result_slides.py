# encoding: utf-8

"""
creates three slides for a module for a single condition
the condition and module index is specified so that it can be displayed on the slide
"""

import pandas as pd
from pptx import Presentation
from pptx.util import Inches
from pptx.util import Pt
from pptx.enum.shapes import MSO_SHAPE


class ProfileSimulationResults(object):
    """
    Class for generating 3 slides for results of one whole simulation run
    """

    def __init__(
        self,
        df_left: pd.DataFrame,
        df_right: pd.DataFrame,
        image_paths: dict,
        prs: Presentation,
        condition: str,
        module_idx: int,
        drive_profile: str ,
        title_and_content_slide_layout_index=None) -> object:
        self._df_left = df_left
        self._df_right = df_right
        self._image_paths = image_paths
        self._condition = condition
        self._slide_count = 0 if condition == "BOL" else 3
        self._module_idx = module_idx
        self._drive_profile= drive_profile
        self._prs = prs
        self._title_and_content_slide_layout = prs.slide_layouts[
            title_and_content_slide_layout_index
        ]
        self._generate_isometric_temp_time_slide()
        self._generate_current_impedance_slide()
        self._generate_power_energy_slide()

    def _set_shapes_for_new_slide(self):
        slide = self._prs.slides.add_slide(self._title_and_content_slide_layout)
        self._slide_count += 1
        self._shapes = slide.shapes
        self._shapes.title.text = (
            f"Simulation Result (EVA2 cell block) -{self._condition}"
        )
        self._add_subtitle()

    def _add_subtitle(self):
        subtitle = self._shapes.add_textbox(
            Inches(0.75), Inches(1), Inches(11), Inches(0.5)
        )
        subtitle.text = f"{self._drive_profile[0:-4]} {self._slide_count} of 6"

    def _generate_isometric_temp_time_slide(self):
        self._set_shapes_for_new_slide()
        self._add_left_shape_isometric_temp_time_slide()
        self._add_right_shape_isometric_temp_time_slide()
        self._add_left_pic_isometric_temp_time_slide()
        self._add_right_pic_isometric_temp_time_slide()
        self._add_table_left_isometric_temp_time_slide()
        self._add_table_right_isometric_temp_time_slide()

    def _add_left_shape_isometric_temp_time_slide(self):
        left = Inches(0.5)
        top = Inches(1.5)
        width = Inches(5.91)
        height = Inches(3.54)
        shape = self._shapes.add_shape(MSO_SHAPE.RECTANGLE, left, top, width, height)
        shape.text = "Figure 1"
        shape.fill.background()

    def _add_right_shape_isometric_temp_time_slide(self):
        left = Inches(0.5 + 5.91)
        top = Inches(1.5)
        height = Inches(3.54)
        width = Inches(5.91)
        shape = self._shapes.add_shape(MSO_SHAPE.RECTANGLE, left, top, width, height)
        shape.text = "Figure 2"
        shape.fill.background()

    def _add_left_pic_isometric_temp_time_slide(self):
        file_path = (
            self._image_paths["image_file_path"] + self._image_paths["isometric_3d"]
        )
        self._shapes.add_picture(
            file_path, Inches(0.51), Inches(1.51), Inches(5.89), Inches(3.52)
        )

    def _add_right_pic_isometric_temp_time_slide(self):
        file_path = (
            self._image_paths["image_file_path"] + self._image_paths["temp_over_time"]
        )
        self._shapes.add_picture(
            file_path, Inches(0.51 + 5.91), Inches(1.51), Inches(5.89), Inches(3.52)
        )

    def _add_table_left_isometric_temp_time_slide(self):
        rows = 7
        cols = 2
        left = Inches(0.5)
        top = Inches(5.05)
        width = Inches(5.90)
        height = Inches(2.5)
        table = self._shapes.add_table(rows+1, cols, left, top, width, height).table
        table.columns[0].width = Inches(2.95)
        table.columns[1].width = Inches(2.95)
        for row_idx in range(rows):
            table.rows[row_idx].height = Inches(0.25)
            table.cell(row_idx+1, 0).text_frame.paragraphs[0].text = str(
                self._df_left.iloc[row_idx]["variable"]
            )
            table.cell(row_idx+1, 1).text_frame.paragraphs[0].text = str(
                self._df_left.iloc[row_idx][self._drive_profile]
            )
            for col_idx in range(cols):
                table.cell(row_idx, col_idx).text_frame.paragraphs[0].font.size = Pt(10)

        for col_idx in range(cols):
                table.cell(rows, col_idx).text_frame.paragraphs[0].font.size = Pt(10)
        table.rows[rows].height = Inches(0.25)
        table.cell(0, 0).text = self._drive_profile
        table.cell(0, 1).text = "Value"

    def _add_table_right_isometric_temp_time_slide(self):
        rows = 6
        cols = 2
        left = Inches(0.5 + 5.91)
        top = Inches(5.05)
        width = Inches(5.90)
        height = Inches(2.5)
        # create table with one extra row to account for header.
        table = self._shapes.add_table(rows+1, cols, left, top, width, height).table
        table.columns[0].width = Inches(4)
        table.columns[1].width = Inches(1.90)
        for row_idx in range(rows):
            for col_idx in range(cols):
                table.cell(row_idx, col_idx).text_frame.paragraphs[0].font.size = Pt(10)
            table.rows[row_idx].height = Inches(0.25)
            table.cell(row_idx+1, 0).text_frame.paragraphs[0].text = str(
                self._df_right.iloc[row_idx]["variable"]
            )
            table.cell(row_idx+1, 1).text_frame.paragraphs[0].text = str(
                self._df_right.iloc[row_idx][self._drive_profile]
            )

        table.cell(0, 0).text = self._drive_profile
        table.cell(0, 1).text = "Value"
        for col_idx in range(cols):
            table.cell(rows, col_idx).text_frame.paragraphs[0].font.size = Pt(10)
        table.rows[rows].height = Inches(0.25)

    def _generate_current_impedance_slide(self):
        self._set_shapes_for_new_slide()
        self._add_left_shape_current_impedance_slide()
        self._add_right_shape_current_impedance_slide()
        self._add_left_pic_current_impedance_slide()
        self._add_right_pic_current_impedance_slide()

    def _add_left_shape_current_impedance_slide(self):
        left = Inches(0.75)
        top = Inches(1.5)
        width = Inches(5.75)
        height = Inches(5.5)
        shape = self._shapes.add_shape(MSO_SHAPE.RECTANGLE, left, top, width, height)
        shape.text = "Figure 1"
        shape.fill.background()

    def _add_right_shape_current_impedance_slide(self):
        left = Inches(7.00)
        top = Inches(1.5)
        height = Inches(5.5)
        width = Inches(5.75)
        shape = self._shapes.add_shape(MSO_SHAPE.RECTANGLE, left, top, width, height)
        shape.text = "Figure 2"
        shape.fill.background()

    def _add_left_pic_current_impedance_slide(self):
        file_path = (
            self._image_paths["image_file_path"] + self._image_paths["driving_profile"]
        )
        self._shapes.add_picture(
            file_path, Inches(0.77), Inches(1.51), Inches(5.72), Inches(5.48)
        )

    def _add_right_pic_current_impedance_slide(self):
        file_path = (
            self._image_paths["image_file_path"] + self._image_paths["cell_resistance_voltage_soc"]
        )
        self._shapes.add_picture(
            file_path, Inches(7.01), Inches(1.51), Inches(5.72), Inches(5.48)
        )

    def _generate_power_energy_slide(self):
        self._set_shapes_for_new_slide()
        self._add_left_shape_power_energy_slide()
        self._add_left_pic_power_energy_slide()
        self._add_right_shape_power_energy_slide()
        self._add_right_pic_power_energy_slide()

    def _add_left_shape_power_energy_slide(self):
        left = Inches(0.75)
        top = Inches(1.5)
        height = Inches(5.5)
        width = Inches(5.75)
        shape = self._shapes.add_shape(MSO_SHAPE.RECTANGLE, left, top, width, height)
        shape.text = "Figure 5"
        shape.fill.background()

    def _add_left_pic_power_energy_slide(self):
        file_path = (
            self._image_paths["image_file_path"] + self._image_paths["power"]
        )
        self._shapes.add_picture(
            file_path, Inches(0.77), Inches(1.52), Inches(5.72), Inches(5.48)
        )

    def _add_right_shape_power_energy_slide(self):
        left = Inches(7.00)
        top = Inches(1.5)
        height = Inches(5.5)
        width = Inches(5.75)
        shape = self._shapes.add_shape(MSO_SHAPE.RECTANGLE, left, top, width, height)
        shape.text = "Figure 2"
        shape.fill.background()

    def _add_right_pic_power_energy_slide(self):
        file_path = (
                self._image_paths["image_file_path"] + self._image_paths["energy"]
        )
        self._shapes.add_picture(
            file_path, Inches(7.01), Inches(1.51), Inches(5.72), Inches(5.48)
        )
