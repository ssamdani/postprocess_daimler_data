# encoding: utf-8

"""
create slides for multiple modules or drive profiles
"""

import os
from pptx import Presentation
from src import simulation_result_slides


class MultipleSlideDecks(object):
    """
    Class for generating slides for all simulation conditions and modules
    """

    def __init__(self, inputs):
        self._inputs = inputs
        number_of_modules = int(inputs.input_config["number_of_modules"])
        for dp in inputs.input_config["drive_profile"]:
            # initialize template for every drive profile
            self.read_presentation_template()
            # set drive profile as it will be used to locate images and create file names
            self.drive_profile = dp
            for condition in self._inputs.input_config["conditions"]:
                # the directory structure is expected in this format: DriveProfile_Condition
                drive_profile_dir = self.drive_profile + "_" + condition

                # TODO code is not written with multiple modules in mind.. this will need to be changed
                for mod_idx in range(number_of_modules):
                    self._generate_slides(mod_idx, condition, drive_profile_dir)
            self._write_final_file()

    def _generate_slides(self, mod_idx, condition, drive_profile_dir):
        # add another key to the file name dictionary with the directory
        self._inputs.file_name_dict["image_file_path"] = (
            self._inputs.input_config["image_file_path"] + drive_profile_dir + "/"
        )
        left_df = self._create_left_table_df(drive_profile_dir)
        right_df = self._create_right_table_df(drive_profile_dir)
        simulation_result_slides.ProfileSimulationResults(
            left_df,
            right_df,
            self._inputs.file_name_dict,
            self.prs,
            condition,
            mod_idx,
            drive_profile_dir,
            self._inputs.input_config["title_and_content_slide_layout_index"],
        )

    def _create_left_table_df(self, drive_profile_dir):
        """ TODO: left table needs to be generated based on module, condition and drive profile
        """
        return self._inputs.df_profile_left_table[["variable", drive_profile_dir]]

    def _create_right_table_df(self, drive_profile_dir):
        """ TODO: left table needs to be generated based on module, condition and drive profile
        """
        return self._inputs.df_profile_right_table[["variable", drive_profile_dir]]

    def read_presentation_template(self):
        """ set the prs variable to a fresh copy of the template"""
        self.prs = Presentation(self._inputs.input_config["presentation_template"])

    def _write_final_file(self):
        file_path = (
            self._inputs.input_config["output_dir"] + self.drive_profile + ".pptx"
        )
        self.prs.save(file_path)
        # open file
        if self._inputs.input_config["open_ppt_on_completion"]:
            os.startfile(file_path, "open")
