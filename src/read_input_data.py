# -*- coding: utf-8 -*-

"""
Read input data for all results to be written
Input data should be provided in config.json file
"""

import json

import pandas as pd


class InputParameters(object):
    """
    read configuration data for where charts/table data are located, number of profiles and name for output presentation
    """

    def __init__(self, input_file_name: str) -> object:
        with open(input_file_name) as f:
            self.input_config = json.load(f)
        print("read input configuration")
        self._read_profile_tables()
        self._read_image_paths()

    def _read_profile_tables(self):
        """ profile tables are used for the first summary slide for a drive profile"""
        self.df_profile_left_table = pd.read_excel(
            self.input_config["profile_data_file"], sheet_name="left_table"
        )
        self.df_profile_right_table = pd.read_excel(
            self.input_config["profile_data_file"], sheet_name="right_table"
        )

    def _read_image_paths(self):
        """ names of image files
        image file names are standardized for each drive profile and located under the image_file_dir
        folder with the drive profile and condition name, example: `Boost_Slow_BOL`
        """
        self.file_name_dict = self.input_config["image_file_names"]
