# -*- coding: utf-8 -*-
"""
This script imports necessary modules to read data from an input config.json file,
generate the slides from the inputs provided and open the last  file upon completion


"""

from src import read_input_data
from src import multiple_slide_decks


def main(input_file_name='config.json'):
    print('Starting reading inputs')
    inputs = read_input_data.InputParameters(input_file_name)

    print('Starting slide creation')
    mps = multiple_slide_decks.MultipleSlideDecks(inputs)

    print('Starting file writing')


if __name__ == "__main__":
    # execute only if run as a script
    main()
