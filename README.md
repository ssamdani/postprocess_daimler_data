Thermal simulation post processor

This tool generates PowerPoint files using the charts generated in thermal_ecm_charts tool 

Here are the steps to use this tool

Step 1
Download the repo to a folder

Step 2
Launch anaconda prompt and cd into the folder where code is downloaded.
Install the required libraries using  
$conda env create -f environment.yml

Step 3
Libraries will get installed in an environment called daimler_plots.
Activate the environment using 
$ activate daimler_plots

Step 3
Modify the config.json file with the appropriate input/output file locations

Step 4
run the script by using following command
$ python runall.py config.json

Step 5
verify results in the folder specified in the config.json
